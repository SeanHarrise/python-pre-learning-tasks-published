def factors(number):
    # ==============
    listOfFactors = []
    for x in range (2, number):
      if number % x == 0:
        listOfFactors.append(x)

    if len(listOfFactors) > 0:
        return listOfFactors
    else:
        return str(number) + ' is a prime number'

    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
