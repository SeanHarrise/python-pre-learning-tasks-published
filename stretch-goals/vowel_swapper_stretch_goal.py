def vowel_swapper(string):

    # Calculate number of occurences of the letter a in the string, by adding
    # upper case and lower case occurences
    lower = string.count('a')
    upper = string.count('A')
    # If there are 2 or more occurrences of the letter a, then the second will
    # need to be swapped to a 4
    if upper + lower >= 2:
        # If all the as are capital then replace the second uppercase A
        if string.count('a') == 0:
          first = string.find('A')
          second = string.find('A', first+1)
          string = string[:second] + '4' + string[second+1:]
        # If all the as are lowercase then replace the second lowercase a
        elif string.count('A') == 0:
          first = string.find('a')
          second = string.find('a', first+1)
          string = string[:second] + '4' + string[second+1:]
        # If there are capital and lowercase letters then you will need to work
        # out where the second letter is and whether it is lowercase or uppercase
        else:
          first = min(string.find('a'), string.find('A'))

          if string.find('a', first+1) < 0:
              second = string.find('A', first+1)
          elif string.find('A', first+1) < 0:
              second = string.find('a', first+1)
          else:
              second = min(string.find('a', first+1), string.find('A', first+1))
          string = string[:second] + '4' + string[second+1:]

    # Calculate number of occurences of the letter a in the string, by adding
    # upper case and lower case occurences
    lower = string.count('e')
    upper = string.count('E')
    # If there are 2 or more occurrences of the letter a, then the second will
    # need to be swapped to a 4
    if upper + lower >= 2:
        # If all the as are capital then replace the second uppercase A
        if string.count('e') == 0:
          first = string.find('E')
          second = string.find('E', first+1)
          string = string[:second] + '3' + string[second+1:]
        # If all the as are lowercase then replace the second lowercase a
        elif string.count('E') == 0:
          first = string.find('e')
          second = string.find('e', first+1)
          string = string[:second] + '3' + string[second+1:]
        # If there are capital and lowercase letters then you will need to work
        # out where the second letter is and whether it is lowercase or uppercase
        else:
          first = min(string.find('e'), string.find('E'))

          if string.find('e', first+1) < 0:
              second = string.find('E', first+1)
          elif string.find('E', first+1) < 0:
              second = string.find('e', first+1)
          else:
              second = min(string.find('e', first+1), string.find('E', first+1))
          string = string[:second] + '3' + string[second+1:]

    # Calculate number of occurences of the letter a in the string, by adding
    # upper case and lower case occurences
    lower = string.count('i')
    upper = string.count('I')
    # If there are 2 or more occurrences of the letter a, then the second will
    # need to be swapped to a 4
    if upper + lower >= 2:
        # If all the as are capital then replace the second uppercase A
        if string.count('i') == 0:
          first = string.find('I')
          second = string.find('I', first+1)
          string = string[:second] + '!' + string[second+1:]
        # If all the as are lowercase then replace the second lowercase a
        elif string.count('I') == 0:
          first = string.find('i')
          second = string.find('i', first+1)
          string = string[:second] + '!' + string[second+1:]
        # If there are capital and lowercase letters then you will need to work
        # out where the second letter is and whether it is lowercase or uppercase
        else:
          first = min(string.find('i'), string.find('I'))

          if string.find('i', first+1) < 0:
              second = string.find('I', first+1)
          elif string.find('I', first+1) < 0:
              second = string.find('i', first+1)
          else:
              second = min(string.find('i', first+1), string.find('I', first+1))
          string = string[:second] + '!' + string[second+1:]

    # Calculate number of occurences of the letter a in the string, by adding
    # upper case and lower case occurences
    lower = string.count('o')
    upper = string.count('O')
    # If there are 2 or more occurrences of the letter a, then the second will
    # need to be swapped to a 4
    if upper + lower >= 2:
        # If all the as are capital then replace the second uppercase A
        if string.count('o') == 0:
          first = string.find('O')
          second = string.find('O', first+1)
          string = string[:second] + '000' + string[second+1:]
        # If all the as are lowercase then replace the second lowercase a
        elif string.count('O') == 0:
          first = string.find('o')
          second = string.find('o', first+1)
          string = string[:second] + 'ooo' + string[second+1:]
        # If there are capital and lowercase letters then you will need to work
        # out where the second letter is and whether it is lowercase or uppercase
        else:
          first = min(string.find('o'), string.find('O'))

          if string.find('o', first+1) < 0:
              second = string.find('O', first+1)
              string = string[:second] + '000' + string[second+1:]
          elif string.find('O', first+1) < 0:
              second = string.find('o', first+1)
              string = string[:second] + 'ooo' + string[second+1:]
          else:
              second = min(string.find('o', first+1), string.find('O', first+1))
              if second == string.find('o', first+1):
                  string = string[:second] + 'ooo' + string[second+1:]
              else:
                  string = string[:second] + '000' + string[second+1:]

    # Calculate number of occurences of the letter a in the string, by adding
    # upper case and lower case occurences
    lower = string.count('u')
    upper = string.count('U')
    # If there are 2 or more occurrences of the letter a, then the second will
    # need to be swapped to a 4
    if upper + lower >= 2:
        # If all the as are capital then replace the second uppercase A
        if string.count('u') == 0:
          first = string.find('U')
          second = string.find('U', first+1)
          string = string[:second] + '|_|' + string[second+1:]
        # If all the as are lowercase then replace the second lowercase a
        elif string.count('U') == 0:
          first = string.find('u')
          second = string.find('u', first+1)
          string = string[:second] + '|_|' + string[second+1:]
        # If there are capital and lowercase letters then you will need to work
        # out where the second letter is and whether it is lowercase or uppercase
        else:
          first = min(string.find('u'), string.find('U'))

          if string.find('u', first+1) < 0:
              second = string.find('U', first+1)
          elif string.find('U', first+1) < 0:
              second = string.find('u', first+1)
          else:
              second = min(string.find('u', first+1), string.find('U', first+1))
          string = string[:second] + '|_|' + string[second+1:]

    return string

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
