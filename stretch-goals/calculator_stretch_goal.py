def calculator(a, b, operator):
    # ==============
    result = 0
    if operator == '+':
        result = a + b
    elif operator == '-':
        result = a - b
    elif operator == '*':
        result = a * b
    elif operator == '/':
        result = a / b

    list = []

    while result > 0:
        list.append(result % 2) # Find the remainder when divided by 2 and add to list
        result = result // 2

    binaryResult = 0

    for x in range (len(list)):
        binaryResult += 10**(x) * list[x]

    print(list)

    return int(binaryResult)

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
